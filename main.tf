provider "aws" {
  profile = "default"
  region = "eu-west-2"
}

resource "aws_ecs_cluster" "foo" {
  name = "my-ecs"
}

resource "aws_ecs_service" "fooservice" {
  name = "my-ecs-service"
  cluster = aws_ecs_cluster.foo.id
  task_definition = aws_ecs_task_definition.web.arn
  desired_count = 1
  launch_type = "EC2"
}

resource "aws_ecs_task_definition" "web" {
  family                = "service"
  container_definitions = file("service.json")

  volume {
    name      = "service-storage"
    host_path = "/ecs/service-storage"
  }
}
